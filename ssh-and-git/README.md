## SSH & Git

Agar saya lebih leluasa dalam melakukan perubahakan pada repository git. Saya membutuhkan **SSH**. Dia menggantikan sistem login yang dimiliki **git-cli** (menggunakan username & password).

Praktek yang akan saya mulai yaitu membuat **SSH Key**. Jalankan perintah dibawah ini dan ikuti instruksi yang ada.
```
ssh-keygen
```

Setelah **SSH Key** dibuat, coba tampilkan isi dari SSH yang sudah dibuat.
<img src="ss/0.png" align="center">

**Note** : Gambar saya blur untuk tujuan keamanan. Mohon dimaklumi.

Saya akan menambahkan **SSH Key** pada akun Gitlab saya. Masuk ke menu <code>Settings > SSH Keys</code>.
<img src="ss/2.png" align="center">

Setelah selesai ditambahakan.
<img src="ss/3.png" align="center">

Cek apakah gitlab sudah terkoneksi dengan PC server saya dengan perintah <code>ssh -T git@gitlab.com</code>, jika benar akan mendapatkan output seperti gambar dibawah.

<img src="ss/1.png" align="center">

Fork repository <a href="https://github.com/sgnd/dumbplay">Dumbplay</a> dari Github ke Gitlab (entah ini cara yang benar atau tidak). Copy linknya
<img src="ss/4.png" align="center">

Klik <code>Add Project > Import Project > Repo from URL</code>.
<img src="ss/5.png" align="center">

Setelah berhasil, maka project sudah masuk diakun Gitlab saya.
<img src="ss/6.png" align="center">

Coba copy link dari menu **Clone with SSH**.
<img src="ss/7.png" align="center">

Masuk ke terminal server dan clone repository.

<img src="ss/8.png" align="center">

Setelah clone sudah selesai, saya akan mencoba **Pull**, untuk itu saya harus melakukan editing file terlebih dahulu pada komputer yang berbeda.
<img src="ss/9.png" align="center">

Lalu coba <code>git pull</code>

<img src="ss/10.png" align="center">

Selanjutnya saya akan mencoba melakukan <code>git push</code> dari komputer server saya. Maka dari itu saya akan melakukan perubahan sederhana pada file <code>README.md</code>.

<img src="ss/11.png" align="center">

Masukkan semua perubahan dengan perintah <code>git add .</code>, <code>git commit</code> dan <code>git push</code>
<img src="ss/12.png" align="center">

Pesan yang disampaikan git saat melakukan commit yaitu, nama penulis akan berubah saat melakukan push (saya tidak masalah dengan itu).
Jika keberatan, anda harus mendefinisikan user & email kalian dengan perintah.
```
git config --global user.username "*username"
git config --global user.email "*user@email.com"
```