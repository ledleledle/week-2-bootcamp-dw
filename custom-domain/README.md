## Custom Domain for Backend Application

Custom domain untuk server backend atau juga untuk penyedia API. ip yang digunakan untuk domain API akan sama dengan ip utama yang digunakan. Karena memang server utama yang mengurus reverse proxy hanya 1.<br>
<img src="ss/1.png" align="center">

Lalu edit file <code>/etc/nginx/leon/backend.conf</code>, ubah <code>server_name</code> dari ip public ke custom domain **api.leon.instructype.com**.<br>
<img src="ss/2.png" align="center">

Restart/reload **nginx** untuk mendapatkan perubahan.<br>
```
sudo systemctl reload nginx.service
```

Sebelumnya pada task <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/custom-domain">Deploy Backend</a>. Saya mengubah <code>baseURL</code> dari file <code>api.js</code> pada server frontend dengan IP Reverse Proxy. Karena sekarang saya sudah memiliki domain sendiri, saya akan mengubahnya dari IP Reverse Proxy menjadi custom domain.
<img src="ss/4.png" align="center">

Restart **pm2**.
```
pm2 restart 0
```

Test apakah custom domain sudah berhasil diimplementasikan.
<img src="ss/3.png" align="center">