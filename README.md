# Week-2 Bootcamp-DW

### Week 2 bersama <a href="https://dumbways.id">DumbWays.id</a>

## Daftar Isi
- <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/ssh-and-git">Install Git & SSH Key</a>
- <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/setup-db">Setup Database</a>
- <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/deploy-backend">Deployment Backend Application</a>
- <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/reverse-proxy">Reverse Proxy for Backend Application</a>
- <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/custom-domain">Custom Domain for Backend Application</a>
- <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/ssl-configuration">SSL Configuration Backend Application</a>

## Special thanks 
- Bootcamp Dumbways.id
- Temen" bootcamp
- Dan mentor" tercinta ❤️

## References :
- http://nginx.org/en/docs/http/load_balancing.html
- https://www.digitalocean.com/community/tutorials/how-to-allow-remote-access-to-mysql
- https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql
- https://www.bjornjohansen.com/wildcard-certificate-letsencrypt-cloudflare