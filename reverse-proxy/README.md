## Reverse Proxy for Backend Application

Sebelumnya pada minggu pertama saya sudah melakukannya pada server frontend. Karena saya memakai server yang sama untuk reverse proxy kali ini. Jadi saya tidak akan memasang **nginx** lagi. Dan langsung pada poin pentingnya saja. Link reverse proxy pada server frontend sebelumnya bisa <a hre="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/aws-reverse-proxy">dicek disini</a>

Saya akan masuk ke folder khusus reverse proxy yang sebelumnya sudah saya buat untuk server frontend.
```
cd /etc/nginx/leon
```

Lalu buat file baru, kali ini saya akan menamainya <code>backend.conf</code>
```
sudo nano backend.conf
```

Lalu isi dengan konfigurasi seperti gambar dibawah.<br>
<img src="ss/1.png" align="center">

Lalu reload **nginx** untuk mendapatkan efek dari perubahan yang sudah dilakukan.
```
sudo systemctl reload nginx.service
```

#### Hasilnya
Well... Karena reverse proxy yang saya set kali ini adalah server backend maka tidak ada hasil jelas yang akan ditampilkan, namun saya bisa menunjukkan hasil yang saya dapatkan dari aplikasi **Postman**.
<img src="ss/2.png" align="center">

## Bonus (Challenge)
Pada saat pemberian task mingguan, sang mentor memberikan tantangan pada kami untuk membuat 2 server frontend dengan tujuan backup server atau bisa juga digunakan untuk load balancing. Jadi saya menerima tantangannya.

Sebenarnya caranya cukup simpel sih, pertama buat server ke 2 untuk frontend. Dengan environtment yang sama (bisa dicek <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/aws-app-private">disini</a>).

Lalu pada server utama edit file reverse proxy yang digunakan pada server frontend. Tambahkan <code>upstream</code> (seperti membuat function pada bahasa pemrograman). Masukkan ip private dari server frontend 1 dan 2. Lalu pada <code>proxy_pass</code> panggil function yang sudah didefinisikan diatas.<br>
<img src="ss/3.png" align="center">

**Note** : Untuk challenge ini, sebelumnya saya sudah memakai custom domain.

Untuk test, saya akan mematikan salah satu server.
<img src="ss/4.png" align="center">

Domain masih bisa dibuka walau durasi proses load website bertambah, setidaknya website masih bisa dibuka.
<img src="ss/5.png" align="center">