## SSL Configuration for Backend Application

Setelah semuanya sudah dipastikan benar. Waktunya untuk memasang SSL. Karena kemarin terdapat error saat sesudah memasang SSL, maka cara yang saya gunakan akan berbeda dari yang sebelumnya (server frontend minggu kemarin). Yaitu menggunakan *Certbot wildcard*.

Pertama ubah mode **Proxied** menjadi **DNS Only** pada menu DNS Cloudflare.
<img src="ss/1.png" align="center">

Lalu dapatkan API token yang dibutuhkan oleh metode certbot wildcard ini dengan cara masuk ke menu <code>My Profile > API Tokens</code>. **View Global API Key**. Setelah itu copy key nya.
<img src="ss/2.png" align="center">

Buka server reverse proxy melalui SSH. Buat folder dalam directory **/root** yang bernama **.secrets**. Didalam folder tersebut buat juga file yang bernama **cloudflare.ini**.
```
sudo su
mkdir /root/.secrets
nano /root/.secrets/cloudflare.ini
```
Isi file <code>cloudflare.ini</code> dengan format seperti dibawah
```
dns_cloudflare_email = "email@saya.com"
dns_cloudflare_api_key = "api_key_yang_sudah_dicopy"
```

Karena file ini akan sangat rahasia, maka diperlukan akses root untuk melihatnya, berikan perimission yang sesuai untuk itu.
```
sudo chmod 0700 /root/.secrets
sudo chmod 0400 /root/.secrets/cloudflare.ini
```

Untuk installasi certbot bisa dilihat pada praktek saya sebelumnya <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/ssl-configuration">diminggu pertama</a>.

Pasang SSL dengan perintah certbot seperti dibawah.
```
sudo certbot certonly --dns-cloudflare --dns-cloudflare-credentials /root/.secrets/cloudflare.ini -d leon.instructype.com,*.leon.instructype.com --preferred-challenges dns-01
```

Cek sertifikat SSL yang sudah dipasang.
```
sudo certbot certificates
```
<img src="ss/3.png" align="center">

Cek pada file reverse proxy yang digunakan pada server backend. Jika belum ada perubahan secara otomatis pada file tersebut, copy konfigurasi pada file reverse proxy server frontend. Dan hapus bagian <code>listen [::]:443 ssl ipv6only=on;</code> pada kedua file reverse proxy, karena akan menyebabkan error pada **nginx**.<br>
<img src="ss/4.png" align="center">

Reload **nginx**.
```
sudo systemctl reload nginx.service
```

Sebelumnya saya mengkonfigurasi domain untuk API yang masih mengarah pada domain <code>http</code>. Untuk itu buka server frontend dan edit file <code>api.js</code>.<br>
<img src="ss/5.png" align="center">

Simpan dan restart **pm2**.
```
pm2 restart 0
```

Sebelumnya saya juga telah mengirimkan **POST** user dari aplikasi **Postman**. Untuk sekarang saya akan mencoba login ke dalam website yang sudah dideploy.
<img src="ss/6.png" align="center">

Jika sudah berhasil login, maka itu sudah menandakan bahwa konfigurasi yang dilakukan sudah benar.

Lalu untuk info sertifikat SSL pada domain api. Hasilnya seperti berikut.
<img src="ss/7.png" align="center">