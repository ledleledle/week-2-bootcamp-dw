## Setup DB MySQL Server

Dalam task kali ini, saya melakukan konfigurasi database, ketentuannya yaitu database dan backend tidak dijadikan dalam satu server dan database dapat diakses dari server backend. Database yang dibutuhkan yaitu MySQL Server

<a href="https://dev.mysql.com/downloads/repo/apt">Download MySQL Community pada link ini</a>. Namun tidak didownload seperti biasanya, saya hanya ingin mendapatkan link downloadnya saja karena saya akan mengunduhnya dalam server database melalui <code>wget</code>.
<img src="ss/1.png" align="center">

Download MySQL melalui <code>wget</code>.<br>
<img src="ss/2.png" align="center">

Install paket yang sudah didownload. Paket ini hanya menyimpan link repository dari MySQL saja.
```
sudo dpkg -i mysql-apt-config_0.8.15-1_all.deb
```

Lalu jalankan system update & upgrade.
```
sudo apt update && sudo apt upgrade -y
```

Setelah proses update & upgrade, proses selanjutnya yaitu pemasangan paket MySQL Server.
```
sudo apt install mysql-server
```

Akan muncul pop-up menu seperti pada gambar dibawah. Pilih menu <code>MySQL Server & Cluster</code> untuk memilih versi yang diinginkan.<br>
<img src="ss/3.png" align="center">

Dalam task ini, saya menggunakan MySQL Server versi 5.7. Setelah semuanya sudah dipastikan benar, tekan **Ok** untuk mengkonfirmasi.<br>
<img src="ss/4.png" align="center">

Masukkan password root yang akan digunakan nantinya.<br>
<img src="ss/5.png" align="center">

Setelah installasi selesai, cek apakah service MySQL sudah berjalan dengan benar atau belum.
```
sudo systemctl status mysql
```
Jika sudah, maka tertulis status <code>Active (running)</codde>.
<img src="ss/6.png" align="center">

Masuk ke console MySQL dengan user root, lalu buat user baru. User baru ini akan digunakan untuk mengakses database dari server backend. Jadi untuk host ip dari user baru ini, akan menggunakan ip private dari server backend. Berikan juga akses selayaknya user root.
```
mysql -u root -p
```
<img src="ss/7.png" align="center">

```
CREATE USER 'leon'@'172.31.80.70' IDENTIFIED BY 'leon';
GRANT ALL PRIVILEGES ON * . * TO 'leon'@172.31.80.70';
FLUSH PRIVILEGES;
exit;
```

Edit juga file konfigurasi MySQL yang terletak pada <code>/etc/mysql/mysql.conf.d/mysqld.cnf</code>. Ubah **bind-address** ke ip private server database. Simpan file lalu restart service **mysql**.<br>
<img src="ss/8.png" align="center">

```
sudo systemctl restart mysql
```

Test apakah konfigurasi yang dilakukan sebelumnya berhasil dengan cara, masuk ke server backend lalu install MySQL Client.
```
sudo apt install mysql-client
```

Akses akun yang sudah dibuat sebelumnya.
```
mysql -u leon -h 172.31.29.22 -p
```
<img src="ss/9.png" align="center">

### PS
Sebenarnya saya membuat script sederhana untuk mempermudah installasi paket.<br>
- https://gitlab.com/ledleledle/auto-install-app