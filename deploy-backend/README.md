## Deployment Backend Application

Pada task sebelumnya saya sudah memasang MySQL Server pada server database, dan saya juga memasang MySQL Client pada server backend, jadi saya hanya akan menggunakan MySQL Client untuk menambahkan database. Dan disambung dengan deployment aplikasi Node.Js khususnya pada bagian **backend**.

Login terlebih dahulu dengan user yang sudah dibuat sebelumnya. Dan buat satu database yang bernama **dumbplay**.
```
mysql -u leon -h 172.31.29.33 -p
CREATE DATABASE dumbplay;
```

Cek apakah database sudah berhasail dibuat dengan perintah.
```
SHOW DATABASES;
```

Jika berhasil database akan muncul pada list. Jika sudah keluar dari console MySQL.<br>
<img src="ss/1.png" align="center">

Clone repository **Dumbplay** yang sudah di fork sebelumnya. Dan masuk folder backend.
```
git clone git@gitlab.com:ledleledle/dumbplay.git
cd dumbplay/backend
```

Install Node.Js dan jalankan <code>apt update & upgrade</code>.
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt update && sudo apt upgrade -y
sudo apt-get install -y nodejs
```

Install **sequelize-cli** dan **pm2** secara global.
```
sudo npm install -g pm2 sequelize-cli
```

Sebelum melakukan migrasi database, ada konfigurasi yang harus diubah. Edit file <code>config/config.js</code>, sesuaikan dengan ip private dari server database.<br>
<img src="ss/2.png" align="center">

Lalu copy dan ubah nama file <code>.env-copy ==> .env</code>
```
cp .env-copy .env
```

Setelah semua sudah dikonfigurasi dengan benar, saatnya bagi saya untuk melakukan migrasi data ke database dengan perintah **seuelize**.
```
sequelize db:migrate
```
<img src="ss/3.png" align="center">

Jangan lupa untuk menjalankan perintah <code>npm install</code>. Setelah instalasi paket **npm** selesai, jalankan **pm2**.
```
pm2 start ecosystem.config.js
```
<img src="ss/4.png" align="center">

Perlu diingat bahwa aplikasi frontend membutuhkan akses pada API yang disediakan oleh server backend. Jadi saya akan mengubah konfigurasi pada aplikasi frontend. Kembali ke server frontend, lalu edit file <code>dumbplay/frontend/src/config/api.js</code>.
<img src="ss/5.png" align="center">

Restart **pm2**.
```
pm2 restart 0
```

Untuk kelanjutan dari file <code>api.js</code> yang barusan diedit, bisa dilihat pada halaman <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/reverse-proxy">Reverse Proxy</a> dan <a href="https://gitlab.com/ledleledle/week-2-bootcamp-dw/-/tree/master/custom-domain">Custom Domain</a>.